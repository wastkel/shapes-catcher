﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    [Header("CoinsBehavior")]
    [Range(0, 10)]
    public float speed;

    [Header("GameBehaviour")]
    public float delay = 5f;
    public float counter; // счеткик спавнера
    public GameObject[] coinsPool;// префабы монет
    public Sprite[] coinsPoolSprite;
    public int currentCoin;//id текущей монеты
    public int score;
    public int multiplier = 1;
    public int chanceBonus = 30;
    public List<GameObject> trapPool;
    public GameObject trap;
    public int countBonuses;
    public int countTrap;


    [Header("Canvas")]
    public SpriteRenderer background;
    public Text scoreTxt;
    public GameObject comboText;


    public bool trig;
    public int randomizeIDCoin;
    public List<GameObject> instantiateCoin; //все монеты на сцене 
    BoxCollider2D leftCollider;
    BoxCollider2D rightCollider;
    GameObject player;
    int chanseNeedCoin = 30;
    [SerializeField] Scrolling scrolling;

    [Header("Bonuses")]
    public bool magnitBonus;
    public bool rainBonus;
    public bool immortality;
    public GameObject shield;


    UIController uiController;

    int CountTrap //объявление свойства
    {
        get // аксессор чтения поля
        {
            return countTrap;
        }
        set // аксессор записи в поле
        {
            if (value > 400)
                countTrap = 3;
            else if (value > 200)
                countTrap = 2;
            else if (value > 100)
                countTrap = 1;
            else countTrap = 0;
        }
    }

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        uiController = GetComponent<UIController>();

        if (PlayerPrefs.HasKey("currentSkin"))
            player.GetComponent<SpriteRenderer>().sprite = uiController.scrolling.skins[PlayerPrefs.GetInt("currentSkin")].GetComponent<Image>().sprite;
        else
            PlayerPrefs.SetInt("currentSkin", 0);


        leftCollider = new GameObject("leftCollider").AddComponent<BoxCollider2D>();
        rightCollider = new GameObject("rightCollider").AddComponent<BoxCollider2D>();
        
        Restart();
    }

    void Update()
    {
        
        Colliders();
        scoreTxt.text = score.ToString();

        counter = rainBonus ? counter += Time.deltaTime * 2 : counter += Time.deltaTime;

        if (counter >= delay)
        {
            if (!rainBonus)
            {
                if (Random.Range(0, 100) < chanseNeedCoin)
                    randomizeIDCoin = currentCoin;
                else if (Random.Range(0, 100) < chanceBonus && GameObject.FindGameObjectWithTag("Bonus") == null)
                    randomizeIDCoin = coinsPool.Length - 1;
                else
                    randomizeIDCoin = Random.Range(0, coinsPool.Length - 1);
            }

            GameObject coinClone = Instantiate(coinsPool[randomizeIDCoin],
                Camera.main.ScreenToWorldPoint(new Vector3(Random.Range(50, Screen.width - 50), Screen.height + 100, 10f)),
                Quaternion.EulerAngles(new Vector3(0, 0, Random.Range(0, 359))));
            coinClone.GetComponent<CoinBehaviour>().coinID = randomizeIDCoin;
            instantiateCoin.Add(coinClone);
            counter = 0;
        }


        CountTrap = score;

        if (trapPool.Count < CountTrap && !trig)
        {
            trig = true;
            StartCoroutine(CounterTrap(Random.Range(3, 5)));

        }
    }

    private void Colliders() //определение положения коллайдеров
    {
        leftCollider.size = new Vector2(1, Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height * 2f, 0)).y);
        leftCollider.offset = new Vector2(Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).x - 0.5f, 0);

        rightCollider.size = new Vector2(1, Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height * 2f, 0)).y);
        rightCollider.offset = new Vector2(Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0)).x + 0.5f, 0);
    }

    public void Restart()// луз
    {
        if (!immortality)
        {
            StopAllCoroutines();
            rainBonus = false;
            magnitBonus = false;

            if (PlayerPrefs.GetInt("Max Score") < score)// save records;
            {
                PlayerPrefs.SetInt("Max Score", score);
                Social.ReportScore(score, "CgkIluD29NELEAIQAQ", (bool success) => { });
                Social.ReportScore(PlayerPrefs.GetInt("MaxMoney"), "CgkIluD29NELEAIQAg", (bool success) => { }); 
            }

            foreach (GameObject coin in instantiateCoin)
            {
                Destroy(coin);
            }
            instantiateCoin.Clear();

            ClearAllTrap();
            trig = false;

            PlayerPrefs.SetInt("Money", PlayerPrefs.GetInt("Money") + score);
            PlayerPrefs.SetInt("MaxMoney", PlayerPrefs.GetInt("MaxMoney") + score); 

            ComboText("");
            uiController.maxScore.text = PlayerPrefs.GetInt("Max Score").ToString();
            uiController.money.text = PlayerPrefs.GetInt("Money").ToString();
            SwapCurrentCoin();

            counter = -2; // задержка перед рестартом
            score = 0; // обнуляем текущие очки
        }
    }

    public void SwapCurrentCoin() // замена монеты, которую ловишь
    {
        int randomResult;
        while (true)
        {
            randomResult = Random.Range(0, coinsPool.Length - 1);
            if (randomResult != currentCoin)
            {
                currentCoin = randomResult;
                break;
            }
        }
        background.sprite = coinsPoolSprite[currentCoin];
    }

    public void ComboText(string text)
    {
        comboText.GetComponent<Animation>().Stop();
        comboText.GetComponent<Text>().text = text;
        comboText.GetComponent<Animation>().Play();
    }

    public void BonusActive()
    {
        int bonusId = Random.Range(1, System.Enum.GetNames(typeof(BonusCount)).Length + 1);

        BonusCount b = (BonusCount)(bonusId - 1);//(BonusCount)System.Enum.GetValues(typeof(BonusCount)).GetValue(bonusId - 1);

        //Debug.Log((BonusCount)(bonusId - 1));
        if (PlayerPrefs.GetInt(b.ToString()) > 0)
        {
            switch (b.ToString())
            {
                case "CryptoRain": //cryptrain
                    
                    StopCoroutine(RainStop(0));
                    player.GetComponent<Player>().counterScore = 0;
                    rainBonus = true;
                    randomizeIDCoin = currentCoin;
                    int delayC = 2;

                    if (PlayerPrefs.GetInt("CryptoRain") == 2)
                        delayC = 3;
                    if (PlayerPrefs.GetInt("CryptoRain") == 3)
                        delayC = 4;
                    if (PlayerPrefs.GetInt("CryptoRain") == 4)
                        delayC = 5;
                    if (PlayerPrefs.GetInt("CryptoRain") == 5)
                        delayC = 6;

                    StartCoroutine(RainStop(delayC));
                    ComboText("CryptoRain");
                    break;
                case "ExtraCoins": //scorebonus
                    int plusCoins = 5;

                    if (PlayerPrefs.GetInt("ExtraCoins") == 2)
                        plusCoins = 10;
                    if (PlayerPrefs.GetInt("ExtraCoins") == 3)
                        plusCoins = 15;
                    if (PlayerPrefs.GetInt("ExtraCoins") == 4)
                        plusCoins = 20;
                    if (PlayerPrefs.GetInt("ExtraCoins") == 5)
                        plusCoins = 25;

                    score += plusCoins;
                    ComboText("+ " + plusCoins.ToString());
                    break;
                case "Magnet": //magnet
                    StopCoroutine(MagnitStop(0));
                    int delayM = 2;

                    if (PlayerPrefs.GetInt("Magnet") == 2)
                        delayM = 3;
                    if (PlayerPrefs.GetInt("Magnet") == 3)
                        delayM = 4;
                    if (PlayerPrefs.GetInt("Magnet") == 4)
                        delayM = 5;
                    if (PlayerPrefs.GetInt("Magnet") == 5)
                        delayM = 6;

                    StartCoroutine(MagnitStop(delayM));
                    magnitBonus = true;
                    ComboText("Magnet");
                    break;
                case "Untouchable": //destroy all traps
                    ClearAllTrap();
                    trig = false;
                    ComboText("Untouchable");
                    break;
                case "Immortality": //Immortality
                    StopCoroutine(Immortality(0));

                    int delayI = 2;

                    if (PlayerPrefs.GetInt("Immortality") == 2)
                        delayI = 3;
                    if (PlayerPrefs.GetInt("Immortality") == 3)
                        delayI = 4;
                    if (PlayerPrefs.GetInt("Immortality") == 4)
                        delayI = 5;
                    if (PlayerPrefs.GetInt("Immortality") == 5)
                        delayI = 6;


                    StartCoroutine(Immortality(5));
                    shield.SetActive(true);
                    immortality = true;
                    ComboText("Immortality");
                    break;
            }
        }
        else { BonusActive(); }
    }

    private void ClearAllTrap()
    {
        foreach (GameObject trap in trapPool)
        {
            Destroy(trap);
        }
        trapPool.Clear();
    }

    void InstanceTrap() // исправить респавн
    {
        float minXRandPos = leftCollider.offset.x;
        float maxXRandPos = rightCollider.offset.x;
        //float sizeYRandPos = leftCollider.size.y;

        bool rot = Random.Range(0, 100) <= 50 ? true : false;

        Quaternion angle = rot ? new Quaternion(0, 0, 90, 90) : Quaternion.identity;
        Vector3 pos = new Vector3(Random.Range(minXRandPos * 0.7f, maxXRandPos * 0.7f), Random.Range(-2.5f, 1), 0);

        trapPool.Add(Instantiate(trap, pos, angle));
    }

    IEnumerator RainStop(int durability)
    {
        yield return new WaitForSeconds(durability);
        rainBonus = false;
        StopCoroutine("RainStop");
    }

    IEnumerator MagnitStop(int durability)
    {
        yield return new WaitForSeconds(durability);
        magnitBonus = false;
        StopCoroutine("MagnitStop");
    }

    IEnumerator Immortality(int durability)
    {
        yield return new WaitForSeconds(durability);
        shield.SetActive(false);
        immortality = false;
        StopCoroutine("Immortality");
    }


    IEnumerator CounterTrap(int delay)
    {
        yield return new WaitForSeconds(delay);
        InstanceTrap();
        trig = false;
        StopCoroutine(CounterTrap(0));
    }

    
}

public enum BonusCount
{ CryptoRain, ExtraCoins, Magnet, Untouchable, Immortality }
