﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinBehaviour : MonoBehaviour {

    public float speed;
    public int coinID;
    Rigidbody2D rb;
    public bool bonus;
    GameManager handler;
    GameObject player;

	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
        speed = Camera.main.GetComponent<GameManager>().speed * 100;
        rb.AddForce(new Vector2(Random.Range(-speed * 0.7f, speed * 0.7f), 1 * -speed));
        handler = Camera.main.GetComponent<GameManager>();
        player = GameObject.FindGameObjectWithTag("Player");
    }
	
	void Update ()
    {
        if (handler.magnitBonus && handler.currentCoin == coinID)
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(player.transform.position.x, transform.position.y), 0.05f);
            rb.velocity = new Vector2(0, rb.velocity.y);
        }

        if (transform.position.y <= Camera.main.ScreenToWorldPoint(new Vector3(0, -50, 0)).y)
        {
            
            Camera.main.GetComponent<GameManager>().instantiateCoin.Remove(gameObject);
            Destroy(gameObject);
        }
	}
}
