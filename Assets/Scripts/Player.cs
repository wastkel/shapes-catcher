﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Player : MonoBehaviour {

    Vector3 mousPos;
    GameManager handler;
    public int counterScore;
    UIController uiController;
    [SerializeField] ParticleSystem[] arrParticle;
    public AudioSource audioCatch;
    public AudioSource audioLose;


    void Start ()
    {
        uiController = Camera.main.GetComponent<UIController>();
        handler = Camera.main.GetComponent<GameManager>();
    }
	
	void Update ()
    {
        MousePos();

        if (counterScore >= 6)
        {
            counterScore = 0;
            handler.SwapCurrentCoin();
            //handler.ComboText("Krasava!");
        }

        if (!uiController.pause && !EventSystem.current.IsPointerOverGameObject())
        {
            transform.position = new Vector3(mousPos.x, transform.position.y);
            transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, mousPos.y), 0.075f);
        }
    }

    private void MousePos()
    {
        mousPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousPos.z = 10f;
        mousPos.y = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height * counterScore / 10)).y;
    }

    public void RestartPlayerPosition()
    {
        counterScore = 0;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.transform.tag != "Trap")
        {
            if (handler.currentCoin == col.GetComponent<CoinBehaviour>().coinID || col.GetComponent<CoinBehaviour>().bonus)
            {
                handler.score += handler.multiplier;
                handler.instantiateCoin.Remove(col.gameObject);
                ParticleSystem particleClone = Instantiate(arrParticle[handler.currentCoin], transform.position + new Vector3(0, 1.25f), new Quaternion());
                Destroy(particleClone.GetComponentInParent<Transform>().gameObject, 3);
                if (!handler.GetComponent<GameManager>().rainBonus)
                    counterScore++;
                if (col.GetComponent<CoinBehaviour>().bonus)
                    handler.BonusActive();
                col.GetComponentInChildren<Transform>().gameObject.SetActive(true);
                Destroy(col.gameObject);
                audioCatch.enabled = false;
                audioCatch.enabled = true;
                Camera.main.GetComponentInChildren<Animation>().Stop();
                Camera.main.GetComponentInChildren<Animation>().Play();

            }
            else if (!handler.immortality)
            {
                audioLose.enabled = false;
                audioLose.enabled = true;
                counterScore = 0;
                handler.Restart();
            }
        }

        
    }
}