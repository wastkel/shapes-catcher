﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour {

    SpriteRenderer spriteRenderer;
    BoxCollider2D collider;
    bool activeTrap;
    float counter;
    float delay;
    [SerializeField] ParticleSystem particle;
    [SerializeField] float lifeTime;
    GameManager handler;
    //bool active;


    void Start()
    {
        //active = Random.Range(0, 100) <= 50 ? true : false;
        float randomScale = Random.Range(1, 2f);
        transform.localScale = new Vector3(1, randomScale, 1);
        spriteRenderer = GetComponent<SpriteRenderer>();
        collider = GetComponent<BoxCollider2D>();
        StartCoroutine(StartTrap());
        ParticleSystem.ShapeModule shapeModule = particle.shape;
        shapeModule.radius = randomScale;
        shapeModule = particle.shape;
        handler = Camera.main.GetComponent<GameManager>();
    }
	
	void Update ()
    {
        counter += Time.deltaTime;

        if (counter >= lifeTime + 3 && activeTrap)
        {
            activeTrap = false;
            particle.Play();
            
            StartCoroutine(DestroyTrap());
        }

        if (activeTrap)
            collider.enabled = true;
        else
            collider.enabled = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player" && !handler.immortality)
        {
            Player player = collision.GetComponent<Player>();
            player.audioLose.enabled = false;
            player.audioLose.enabled = true;
            player.counterScore = 0;
            handler.Restart();
        }
    }

    

    IEnumerator StartTrap()
    {
        yield return new WaitForSeconds(3);
        Color color = new Color();
        color = spriteRenderer.color;
        color.a = 1;
        spriteRenderer.color = color;
        activeTrap = true;
        StopCoroutine(StartTrap());
    }

    IEnumerator DestroyTrap()
    {
        yield return new WaitForSeconds(0.5f);
        Color color = new Color();
        color = spriteRenderer.color;
        color.a = 0;
        spriteRenderer.color = color;
        yield return new WaitForSeconds(2.5f);

        var trapPool = handler.trapPool;

        trapPool.Remove(transform.parent.gameObject);
        Destroy(transform.parent.gameObject);
    }
}
