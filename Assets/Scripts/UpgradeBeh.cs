﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeBeh : MonoBehaviour
{

    [SerializeField] GameObject[] panels;
    [SerializeField] Sprite Star;
    [SerializeField] Sprite EmptyStar;
    [SerializeField] Text costText;
    public GameObject selectedBonus;
    int[][] bonusesCost = new int[5][];
    [SerializeField] ToggleGroup togl;

    string[] bonuses = { "CryptoRain", "ExtraCoins", "Magnet", "Untouchable", "Immortality" };


    void Start()
    {

        bonusesCost[0] = new int[5] { 500, 1000, 5000, 10000, 15000 };   // CryptoRain
        bonusesCost[1] = new int[5] { 0, 100, 500, 2500, 10000 };        // ExtraCoins
        bonusesCost[2] = new int[5] { 100, 500, 2500, 5000, 10000 };     // Magnet
        bonusesCost[3] = new int[1] { 5000 };                            // Untouchable
        bonusesCost[4] = new int[5] { 500, 1000, 5000, 10000, 15000 };   // Immortality

        if (!PlayerPrefs.HasKey("ExtraCoins"))
            PlayerPrefs.SetInt("ExtraCoins", 1);
        foreach (var item in bonuses)
        {
            if (!PlayerPrefs.HasKey(item))
                PlayerPrefs.SetInt(item, 0);
        }
    }


    void Update()
    {

    }

    public void RefreshUpgrade()
    {
        foreach (GameObject panel in panels)
        {
            int levelBonus = PlayerPrefs.GetInt(panel.name) + 1;
            Image[] star = panel.GetComponentsInChildren<Image>();
            foreach (Image item in star)
            {
                if (levelBonus == 0)
                    break;
                item.sprite = Star;
                levelBonus--;
            }
        }
    }

    public void SelectedBonus(GameObject selectBonus)
    {
        if (PlayerPrefs.GetInt(selectBonus.name) <= bonusesCost[(int)((BonusCount)System.Enum.Parse(typeof(BonusCount), selectBonus.name))].Length - 1)
        {
            costText.text = /*"Cost : " +*/ bonusesCost[(int)((BonusCount)System.Enum.Parse(typeof(BonusCount), selectBonus.name))][PlayerPrefs.GetInt(selectBonus.name)].ToString();
            selectedBonus = selectBonus;
        }
        else
        {
            costText.text = "Max Level";
        }
    }

    public void BuyUpgrade()
    {
        if (selectedBonus != null)
        {
            if (costText.text != "Max Level")
                if (PlayerPrefs.GetInt("Money") >= int.Parse(costText.text))
                {
                    PlayerPrefs.SetInt(selectedBonus.name, PlayerPrefs.GetInt(selectedBonus.name) + 1);
                    PlayerPrefs.SetInt("Money", PlayerPrefs.GetInt("Money") - int.Parse(costText.text));//bonusesCost[(int)((BonusCount)System.Enum.Parse(typeof(BonusCount), selectedBonus.name))][PlayerPrefs.GetInt(selectedBonus.name)]);
                    RefreshUpgrade();
                    GetComponent<UIController>().money.text = PlayerPrefs.GetInt("Money").ToString();
                    SelectedBonus(selectedBonus);
                }
        }
    }
}
