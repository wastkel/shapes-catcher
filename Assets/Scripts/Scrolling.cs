﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scrolling : MonoBehaviour {

    [Header("Controllers")]
    [Range(0f, 1000f)]
    public int skinOffset = 2;
    [Range(0f, 20f)]
    public float snapSpeed;
    [Range(0f, 10f)]
    public float scaleOffset;
    [Range(1f, 20f)]
    public float scaleSpeed;

    public GameObject[] skins;
    public int[] costSkins;
    Vector2[] skinScale;
    public int selectedSkin;
    public ScrollRect scrollRect;

    RectTransform contentRect;
    Vector2 contentVector;
    bool isScrolling;

    void Start ()
    {
        contentRect = GetComponent<RectTransform>();
        skinScale = new Vector2[skins.Length];
    }
	
	void Update () {
		
	}

    private void FixedUpdate()
    {
        if (contentRect.anchoredPosition.x >= skins[0].transform.localPosition.x && !isScrolling || contentRect.anchoredPosition.x <= skins[skins.Length - 1].transform.localPosition.x && !isScrolling)
            scrollRect.inertia = false;
        float nearestPos = float.MaxValue;
        for (int i = 0; i < skins.Length; i++)
        {
            float distance = Mathf.Abs(contentRect.anchoredPosition.x + skins[i].transform.localPosition.x);
            if (distance < nearestPos)
            {
                nearestPos = distance;
                selectedSkin = i;
            }
            float scale = Mathf.Clamp(1 / (distance / skinOffset) * scaleOffset, 0.5f, 1.5f);
            skinScale[i].x = Mathf.SmoothStep(skins[i].transform.localScale.x, scale + 0.3f, scaleSpeed * Time.fixedDeltaTime);
            skinScale[i].y = Mathf.SmoothStep(skins[i].transform.localScale.y, scale + 0.3f, scaleSpeed * Time.fixedDeltaTime);
            skins[i].transform.localScale = skinScale[i];
        }
        float scrollVelocity = Mathf.Abs(scrollRect.velocity.x);
        if (scrollVelocity < 400 && !isScrolling) scrollRect.inertia = false;
        if (isScrolling || scrollVelocity > 400) return;
        contentVector.x = Mathf.SmoothStep(contentRect.anchoredPosition.x, -skins[selectedSkin].transform.localPosition.x, snapSpeed * Time.fixedDeltaTime);
        contentRect.anchoredPosition = contentVector;
    }

    public void IsScrolling(bool scroll)
    {
        isScrolling = scroll;
    }
}
