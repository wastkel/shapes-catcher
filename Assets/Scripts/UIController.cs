﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using System;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class UIController : MonoBehaviour
{   
    public bool pause;
    public Canvas canvas;
    public GameObject catcher;
    [SerializeField] public Scrolling scrolling;

    [SerializeField] GameObject buyButton;
    [SerializeField] Text costSkin;
    [SerializeField] GameObject chooseButton;
    [SerializeField] GameObject recordButton;

    public Text maxScore;
    public Text money;

    //const string leaderboard = "CgkI7pG9tPccEAIQAQ";


    void Start()
    {
        if (PlayerPrefs.HasKey("GPS"))
        {
            PlayGamesPlatform.Activate();
            Social.localUser.Authenticate((bool success) =>
            {
                if (success) recordButton.GetComponent<Button>().interactable = true;
                else recordButton.GetComponent<Button>().interactable = false;

                //OnConnectionResponse(success);
            });
        }


        //PlayerPrefs.DeleteAll();
        //PlayerPrefs.SetInt("Money", 999999);

        Advertisement.Initialize("2578470");//2578470
        Advertisement.GetPlacementState("rewardedVideo");//video

        PlayerPrefs.SetString("0", "Buy"); 
        maxScore.text = PlayerPrefs.GetInt("Max Score").ToString();
        money.text = PlayerPrefs.GetInt("Money").ToString();
    }


    void Update()
    {
        ShowCost();
    }

    public void AudioOff()
    {
        AudioListener.volume = 0f;
    }

    public void AudioOn()
    {
        AudioListener.volume = 1f;
    }

    public void BuySkin()
    {
        if (PlayerPrefs.GetInt("Money") >= scrolling.costSkins[scrolling.selectedSkin])
        {
            PlayerPrefs.SetInt("Money", PlayerPrefs.GetInt("Money") - scrolling.costSkins[scrolling.selectedSkin]);
            PlayerPrefs.SetString(scrolling.selectedSkin.ToString(), "Buy");
            money.text = PlayerPrefs.GetInt("Money").ToString();
        }
    }

    void ShowCost()
    {
        if (PlayerPrefs.GetString(scrolling.selectedSkin.ToString()) == "")
        {
            buyButton.SetActive(true);
            chooseButton.SetActive(false);
            costSkin.text = scrolling.costSkins[scrolling.selectedSkin].ToString();
        }
        else
        {
            buyButton.SetActive(false);
            chooseButton.SetActive(true);
        }
    }

    public void ChooseSkin()
    {
        catcher.GetComponent<SpriteRenderer>().sprite = scrolling.skins[scrolling.selectedSkin].GetComponent<Image>().sprite;
        PlayerPrefs.SetInt("currentSkin", scrolling.selectedSkin);
    }

    public void ActivatePause()
    {
        pause = !pause;
        if (pause)
            Time.timeScale = 0;
        else
            Time.timeScale = 1;
    }

    public void ExitToMenu()
    {
        ActivatePause();
        canvas.sortingOrder = 2;
    }

    public void Play()
    {
        canvas.sortingOrder = 0;
    }

    private void HandleAdResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                if (PlayerPrefs.GetInt("MaxMoney") < 1100)
                {
                    PlayerPrefs.SetInt("Money", PlayerPrefs.GetInt("Money") + 100);
                    PlayerPrefs.SetInt("MaxMoney", PlayerPrefs.GetInt("MaxMoney") + 100);
                    money.text = PlayerPrefs.GetInt("Money").ToString();
                }
                else if (PlayerPrefs.GetInt("MaxMoney") < 2600)
                {
                    PlayerPrefs.SetInt("Money", PlayerPrefs.GetInt("Money") + 200);
                    PlayerPrefs.SetInt("MaxMoney", PlayerPrefs.GetInt("MaxMoney") + 200);
                    money.text = PlayerPrefs.GetInt("Money").ToString();
                }
                else if (PlayerPrefs.GetInt("MaxMoney") < 13000)
                {
                    PlayerPrefs.SetInt("Money", PlayerPrefs.GetInt("Money") + 900);
                    PlayerPrefs.SetInt("MaxMoney", PlayerPrefs.GetInt("MaxMoney") + 900);
                    money.text = PlayerPrefs.GetInt("Money").ToString();
                }
                else if (PlayerPrefs.GetInt("MaxMoney") < 27500)
                {
                    PlayerPrefs.SetInt("Money", PlayerPrefs.GetInt("Money") + 1750);
                    PlayerPrefs.SetInt("MaxMoney", PlayerPrefs.GetInt("MaxMoney") + 1750);
                    money.text = PlayerPrefs.GetInt("Money").ToString();
                }
                else if (PlayerPrefs.GetInt("MaxMoney") < 50000)
                {
                    PlayerPrefs.SetInt("Money", PlayerPrefs.GetInt("Money") + 2500);
                    PlayerPrefs.SetInt("MaxMoney", PlayerPrefs.GetInt("MaxMoney") + 2500);
                    money.text = PlayerPrefs.GetInt("Money").ToString();
                }
                else
                {
                    PlayerPrefs.SetInt("Money", PlayerPrefs.GetInt("Money") + 3000);
                    PlayerPrefs.SetInt("MaxMoney", PlayerPrefs.GetInt("MaxMoney") + 3000);
                    money.text = PlayerPrefs.GetInt("Money").ToString();
                }
                break;
            case ShowResult.Skipped:
                break;
            case ShowResult.Failed:
                break;
        }
    }

    public void AdvertisementButton()
    {

        if (Advertisement.IsReady())
        {
            Advertisement.Show("rewardedVideo", new ShowOptions() { resultCallback = HandleAdResult });
        }
    }

    public void LeaderBoardButton()
    {
        PlayGamesPlatform.Activate();
        Social.localUser.Authenticate((bool success) =>
        {
            //if (success) recordButton.GetComponent<Button>().interactable = true;
            //else recordButton.GetComponent<Button>().interactable = false;

            if (success)
            {
                PlayerPrefs.SetString("GPS", "OK");
                Social.ShowLeaderboardUI();
            }
        });
    }
}
